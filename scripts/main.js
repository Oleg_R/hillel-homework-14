'use strict'
//Menu
document.querySelector('.main-navigation').onclick = function(event) {
    var target = event.target; 
    console.log(target); 
    if (target.parentElement.className == 'main-navigation_item' && target.className != 'login-link main-navigation_link') {
        var s = target.parentElement.getElementsByClassName('drop_list');
        closeMenu();
        s[0].style.display='block';
    }
}
document.onclick = function(event) {
    var target = event.target;     
    if (target.parentElement.className != 'main-navigation_item' 
    && target.parentElement.className != 'drop_item') {
        closeMenu();                
    }
}
function closeMenu(){    
    var subm = document.getElementsByClassName('drop_list');
    for (var i = 0; i < subm.length; i++) {
        subm[i].style.display="none";
    }
}

//Popup
var bg = document.querySelector(".bg-modal");
var link = document.querySelector(".login-link");
var popup = document.querySelector(".modal-login");
var close = popup.querySelector(".modal-close");

link.addEventListener("click", function (event) {
  closeMenu();  
  event.preventDefault();  
  bg.style.display = 'block';
  popup.classList.add("modal-show");
});

close.addEventListener("click", function (event) {
    closePopup();        
});
window.addEventListener("keydown", function (event) {
    if (event.keyCode === 27) {
        closePopup();        
    }
});
function closePopup(){
    event.preventDefault();
    bg.style.display = 'none';
    if (popup.classList.contains("modal-show")) {
        popup.classList.remove("modal-show");            
    }
}
bg.onclick = function(event) {
    var target = event.target;     
    if (target.className == 'bg-modal') {         
        closePopup();       
    }
}